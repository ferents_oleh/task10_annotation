package com.epam.reflection;

import com.epam.model.Sum;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class SumReflection {
    public static void invokeOverloadMethods(Sum sum) {
        Class<Sum> c = Sum.class;
        try {
            Method intMethod = c.getDeclaredMethod("sum", int.class, int.class);
            System.out.println(intMethod.invoke(sum, 2, 4));
            Method floatMethod = c.getDeclaredMethod("sum", float.class, float.class, float.class);
            System.out.println(floatMethod.invoke(sum, 2.2F, 4.3F, 1.5F));
            Method doubleMethod = c.getDeclaredMethod("sum", double.class, double.class, double.class);
            System.out.println(doubleMethod.invoke(sum, 5.5, 2.3, 6.6));
        } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace();
        }
    }
}
