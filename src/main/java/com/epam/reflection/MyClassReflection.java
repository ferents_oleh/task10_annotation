package com.epam.reflection;

import com.epam.model.MyClass;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class MyClassReflection {
    public static void invokerMyMethods(MyClass myClass) {
        Class<MyClass> c = MyClass.class;
        try {
            Method method1 = c.getDeclaredMethod("myMethod", String.class, int[].class);
            Method method2 = c.getDeclaredMethod("myMethod", String[].class);
            method1.invoke(myClass, "Hello, world!", new int[] {1, 3, 3, 7});
            method2.invoke(myClass, new Object[] {new String[] {"2", "6", "10"}});
        } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace();
        }
    }
}
