package com.epam.reflection;

import com.epam.annotation.CustomAnnotation;
import com.epam.model.User;

import java.lang.reflect.Field;

public class UserReflection {
    public static User printAnnotatedFields(User user) {
        Class<User> c = User.class;
        Field[] fields = c.getDeclaredFields();
        for (Field f : fields) {
            if (f.isAnnotationPresent(CustomAnnotation.class)) {
                f.setAccessible(true);
                CustomAnnotation customAnnotation = f.getAnnotation(CustomAnnotation.class);
                try {
                    f.set(user, customAnnotation.value());
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
            }
        }
        return user;
    }
}
