package com.epam;

import com.epam.model.MyClass;
import com.epam.model.Sum;
import com.epam.model.User;
import com.epam.reflection.MyClassReflection;
import com.epam.reflection.SumReflection;
import com.epam.reflection.UserReflection;

public class Application {
    public static void main(String[] args) {
        System.out.println(UserReflection.printAnnotatedFields(new User()));

        SumReflection.invokeOverloadMethods(new Sum());

        MyClassReflection.invokerMyMethods(new MyClass());
    }
}
